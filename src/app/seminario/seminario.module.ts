import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SeminarioPageRoutingModule } from './seminario-routing.module';

import { SeminarioPage } from './seminario.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SeminarioPageRoutingModule
  ],
  declarations: [SeminarioPage]
})
export class SeminarioPageModule {}
