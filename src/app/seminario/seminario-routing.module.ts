import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SeminarioPage } from './seminario.page';

const routes: Routes = [
  {
    path: '',
    component: SeminarioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SeminarioPageRoutingModule {}
