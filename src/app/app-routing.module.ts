import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'quienes',
    loadChildren: () => import('./quienes/quienes.module').then( m => m.QuienesPageModule)
  },
  {
    path: 'seminario',
    loadChildren: () => import('./seminario/seminario.module').then( m => m.SeminarioPageModule)
  },
  {
    path: 'observatorio',
    loadChildren: () => import('./observatorio/observatorio.module').then( m => m.ObservatorioPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
