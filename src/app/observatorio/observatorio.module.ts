import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ObservatorioPageRoutingModule } from './observatorio-routing.module';

import { ObservatorioPage } from './observatorio.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ObservatorioPageRoutingModule
  ],
  declarations: [ObservatorioPage]
})
export class ObservatorioPageModule {}
