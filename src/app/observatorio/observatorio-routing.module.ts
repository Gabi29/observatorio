import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ObservatorioPage } from './observatorio.page';

const routes: Routes = [
  {
    path: '',
    component: ObservatorioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ObservatorioPageRoutingModule {}
